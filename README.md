# LIS 4930

## Landon Thaler

### Completed Assignments:

1. [Assignment 1](/A1/README.md "README.md")

    - For this assignment we had to install git
    - Install java JDK
    - Android Studio MyFirstApp
    - Android Studio Contacts App
    - Chapter questions

2. [Assignment 2](/A2/README.md "README.md")

	- Backward engineer Tip Calculator
    - Tip Calculator to do the following:
    - Drop-down menu for total number of guests (including yourself): 1 – 10
    - Drop-down menu for tip percentage (5% increments): 0 – 25
    - Chapter 3 questions

3. [Assignment 3](/A3/README.md "README.md")

	- Screenshot of running application’s unpopulated user interface
    - Screenshot of running application’s toast notification
    - Screenshot of running application’s converted currency user interface
    - Display launcher icon
    - Change background color
    - Include Currency signs
    - Chapter 4 questions

4. [Project 1](/P1/README.md "README.md")

    - Screenshot of running application’s splash screen;
    - Screenshot of running application’s follow-up screen (with images and buttons)
    - Screenshots of running application’s play and pause user interfaces (with images and buttons)
    - Display launcher icon
    - Change color scheme
    - Have 3 music files
    - Chapter Questions

5. [Assignment 4](/A4/README.md "README.md")
    - Screenshot of running application’s splash screen 
    - Screenshot of running application’s individual article 
    - Screenshots of running application’s default browser
    - Include splash screen with app title and list of articles
    - Must find and use your own RSS feed.
    - Must add background color(s) or theme
    - Create and display launcher icon image
    - Chapter questions

6. [Assignment 5](/A5/README.md "README.md")
    - Course title, your name, assignment requirements, as per A1
    - Screenshot of running application’s splash screen (list of articles – activity_items.xml)
    - Screenshot of running application’s individual article (activity_item.xml)
    - Screenshots of running application’s default browser (article link)
    - Include splash screen with app title and list of articles.
    - Must find and use your own RSS feed.
    - Must add background color(s) or theme.
    - Create and display launcher icon image
    - Chapter questions

7. [Project 2](/P2/README.md "README.md")
    - Screenshot of running application task list
    - Insert at least five sample tasks 
    - Test database class 
    - Must add background color(s) or theme 
    - Create and display launcher icon image 
    - Chapter questions

