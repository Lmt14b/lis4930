# LIS4930

## Landon Thaler

### Assignment # Requirements:

*Three parts:*

1. Build Task List app
2. Screenshots of app running
3. Questions for assignment

#### README.md file should include the following items:

* Screenshot of Task List app
* description of program


> #### Description of program:

1. Insert at least five sample tasks
2. Test database class 
3. Must add background color(s) or theme
4. Create and display launcher icon image 




#### Assignment Screenshots:

*Screenshot of My music home*:

![tasklist](img/tasklist.png)

