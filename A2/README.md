# LIS4930

## Landon Thaler

### Assignment # Requirements:

*Three parts:*

1. Build tip calculator app
2. Screenshots of app running
3. Questions for assignment

#### README.md file should include the following items:

* Screenshot of Tip calculator
* Screenshot of Tip calculator in use
* description of program


> #### Description of program:

1. Drop-down menu for total number of guests (including yourself): 1 – 10
2. Drop-down menu for tip percentage (5% increments): 0 – 25
3. Must add background color(s) or theme


#### Assignment Screenshots:

*Screenshot of tip calculator home*:

![Home](img/home.png)

*Screenshot of tip calculator running*:

![Running](img/inuse.png)

