# LIS4930

## Landon Thaler

### Assignment # Requirements:

*Three parts:*

1. Build My Music app
2. Screenshots of app running
3. Questions for assignment

#### README.md file should include the following items:

* Screenshot of My Music app
* Screenshot of My Music in use
* description of program


> #### Description of program:

1. Include splash screen image, app title, intro text.
2. Include artists’ images and media.
3. Images and buttons must be vertically and horizontally aligned.
4. Must add background color(s) or theme
5. Create and display launcher icon image



#### Assignment Screenshots:

*Screenshot of My music home*:

![Home](img/home.png)

*Screenshot of My Music running*:

![Running](img/playing.png)

*Screenshot of My Music Splash Screen*:

![Home](img/splash.png)
