# LIS4930

## Landon Thaler

### Assignment # Requirements:

*Three parts:*

1. Build Currency Converter app
2. Screenshots of app running
3. Questions for assignment

#### README.md file should include the following items:

* Screenshot of Currency converter
* Screenshot of Currency Converter in use
* description of program


> #### Description of program:

1. Screenshot of running application’s unpopulated user interface
2. Screenshot of running application’s toast notification
3. Screenshot of running application’s converted currency user interface

#### Assignment Screenshots:

*Screenshot of Currency converter home*:

![Home](img/blank.png)

*Screenshot of Currency converter running*:

![Running](img/working.png)

