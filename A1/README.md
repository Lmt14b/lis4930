# 

## Landon Thaler

### Assignment 1 Requirements:

*Two Parts*

1. Installing and using Git and BitBucket
2. Installing java JDK

#### README.md file should include the following items:

* Screenshot of Java Hello
* Screenshot of My First App
* Screenshot of Contacts App
* Git commands and descriptions

> #### Git commands w/short descriptions:

1. git init - is used to create an empty git repository or reinitialize an existing one 
2. git status - is used to show the working tree status
3. git add - is used to add file contents to the index
4. git commit - is used to record changes to the repository
5. git push - is used to update remote references along with associated objects
6. git pull - is used to fetch from and integrate with another repository or a local branch
7. git rm - is used to remove files from the working tree and from the index

#### Assignment Screenshots:

*Screenshot of Java Hello*:

![ASP.NET](img/asp.png)

*Screenshot of My First App*:

![hwapp](img/helloworld.png)

*Screenshot of Contact App*:

![contact1](img/contactsList.png)
![contact2](img/profile.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Lmt14b/bitbucketstationlocations// "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/Lmt14b/myteamsquotes/ "My Team Quotes Tutorial")