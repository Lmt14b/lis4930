# LIS4930

## Landon Thaler

### Assignment # Requirements:

*Three parts:*

1. Build RSS Feed app
2. Screenshots of app running
3. Questions for assignment

#### README.md file should include the following items:

* Screenshot of RSS Feed App
* Screenshot of News Reader in use
* description of program


> #### Description of program:

1. Course title, your name, assignment requirements, as per A1
2. Screenshot of running application’s splash screen (list of articles – activity_items.xml)
3. Screenshot of running application’s individual article (activity_item.xml)
4. Screenshots of running application’s default browser (article link)
 


#### Assignment Screenshots:

*Screenshot of Mortgage Calculator home*:

![Splash](images/Splash.png)

*Screenshot of Mortgage Calculator running*:

![ReadMore](images/ReadMore.png)

*Screenshot of Mortgage Calculator invalid*:

![Web](images/web.png)

