# LIS4930

## Landon Thaler

### Assignment # Requirements:

*Three parts:*

1. Build Mortgage Calculator app
2. Screenshots of app running
3. Questions for assignment

#### README.md file should include the following items:

* Screenshot of Mortgage Calculator
* Screenshot of Currency Converter in use
* description of program


> #### Description of program:

1. Screenshot of running application’s splash screen 
2. Screenshot of running application’s individual article 
3. Screenshots of running application’s default browser 


#### Assignment Screenshots:

*Screenshot of Mortgage Calculator home*:

![Home](images/Home.png)

*Screenshot of Mortgage Calculator running*:

![Running](images/working.png)

*Screenshot of Mortgage Calculator invalid*:

![Invalid](images/message.png)

